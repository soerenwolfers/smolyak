'''
Polynomial Approximation
'''
from .polynomial_approximation import PolynomialApproximator
from .probability_spaces import ProbabilityDistribution